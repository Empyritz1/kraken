import React, { Component } from "react";
import Header from "../components/Header";
import CardList from "../components/CardList";

class Home extends Component {
  render() {
    return (
      <div>
        <Header />
        <CardList />
      </div>
    );
  }
}

export default Home;
