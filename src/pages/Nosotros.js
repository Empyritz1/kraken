import React, { Component } from "react";
// import { Grid } from "@material-ui/core";
import SimpleTabs from "../components/SimpleTabs";
import {
  BookOpenPageVariant,
  BullseyeArrow,
  Charity,
  EyeCheckOutline
} from "mdi-material-ui";

class Nosotros extends Component {
  render() {
    const info = [
      {
        label: "Historia",
        title: "Historia",
        text:
          "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur culpa corporis sint, veritatis quo voluptatum incidunt totam distinctio nihil. Suscipit ut, sapiente cupiditate repellat culpa architecto exercitationem expedita amet dolores.",
        icon: <BookOpenPageVariant />
      },
      {
        label: "Misión",
        title: "Misión",
        text:
          "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur culpa corporis sint, veritatis quo voluptatum incidunt totam distinctio nihil. Suscipit ut, sapiente cupiditate repellat culpa architecto exercitationem expedita amet dolores.",
        icon: <BullseyeArrow />
      },
      {
        label: "Visión",
        title: "Visión",
        text:
          "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur culpa corporis sint, veritatis quo voluptatum incidunt totam distinctio nihil. Suscipit ut, sapiente cupiditate repellat culpa architecto exercitationem expedita amet dolores.",
        icon: <EyeCheckOutline />
      },
      {
        label: "Valores",
        title: "Valores",
        text:
          "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur culpa corporis sint, veritatis quo voluptatum incidunt totam distinctio nihil. Suscipit ut, sapiente cupiditate repellat culpa architecto exercitationem expedita amet dolores.",
        icon: <Charity />
      }
    ];
    return <SimpleTabs info={info} />;
  }
}

export default Nosotros;
