import React, { Component } from "react";
import InfoPanel from "../components/InfoPanel";
import chaleco from "../images/chaleco.png";
import "./Manual.css";

class Recomendaciones extends Component {
  render() {
    return (
      <div className="manual">
        <div className="manualContainer">
          <InfoPanel className="infoPanel" />
          <div className="manualImg">
            <img src={chaleco} alt="chaleco" width="250px" height="250px" />
          </div>
        </div>
      </div>
    );
  }
}

export default Recomendaciones;
