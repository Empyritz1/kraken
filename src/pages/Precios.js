import React, { Component } from "react";
import Table from "../components/Table";
import { makeStyles } from "@material-ui/core/styles";

function createData(...data) {
  return { ...data };
}

function createHeadData(...data) {
  return [...data];
}

const tipoITitles = createHeadData(
  "Tallas",
  "Kg",
  "Un color",
  "Dos colores",
  "Camuflajeado"
);
const tipoIData = [
  createData("Baby - Bebe", "15", "$175.00", "$189.00", "$184.00"),
  createData("Kid - Niño", "30", "$175.00", "$189.00", "$184.00"),
  createData("XS - ECH", "45", "$189.00", "$206.00", "$200.00"),
  createData("S - CH", "60", "$200.00", "$216.00", "$211.00"),
  createData("M", "75", "$211.00", "$227.00", "$222.00"),
  createData("L - G", "90", "$211.00", "$227.00", "$222.00"),
  createData("XL - EG", "110", "$222.00", "$238.00", "$233.00"),
  createData("XXL - EG", "120", "$233.00", "$249.00", "$243.00"),
  createData("XXXL - EEG", "140", "$254.00", "$270.00", "$265.00")
];

const tipoIITitles = createHeadData(
  "Tallas",
  "Kg",
  "Yugo - Herradura",
  "Chaleco",
  null
);
const tipoIIData = [
  createData("Niño", "Unitalla", "$260.00", "$319.00"),
  createData("Adulto", "Unitalla", "$260.00", "$319.00")
];

const tipoIIITitles = createHeadData("Tallas", "kg", "Jurojin", null, null);
const tipoIIIData = [
  createData("XS - ECH", "45", "$410.00"),
  createData("S - CH", "60", "$410.00"),
  createData("M", "75", "$410.00")
];

const tipoIVTitles = createHeadData("Talla", "kg", "$Fukurokuju", null, null);
const tipoIVData = [
  createData("Niño", "Unitalla", "$89"),
  createData("Adulto", "Unitalla", "$119")
];

class Precios extends Component {
  render() {
    const useStyles = makeStyles({
      tableTitle: {
        alignText: "center"
      }
    });

    const classes = useStyles;

    console.log(tipoIData);
    return (
      <div>
        <Table
          id="tipo1"
          title="Tipo I"
          headCells={tipoITitles}
          data={tipoIData}
        />
        <Table
          id="tipo2"
          title="Tipo II"
          headCells={tipoIITitles}
          data={tipoIIData}
        />
        <Table
          id="tipo3"
          title="Tipo III"
          headCells={tipoIIITitles}
          data={tipoIIIData}
        />
        <Table
          id="tipo4"
          title="Tipo IV"
          headCells={tipoIVTitles}
          data={tipoIVData}
        />
        {/* <Tabla titles={tipoITitles} data={tipoIData} />
        <Tabla titles={tipoIITitles} data={tipoIIData} /> */}
      </div>
    );
  }
}

export default Precios;
