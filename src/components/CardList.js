import React from "react";
import Card from "./Card";

import { makeStyles } from "@material-ui/core/styles";

import chalecoTipoI from "../images/chaleco-salvavidas-tipo-I.jpg";
import chalecoTipoII from "../images/chaleco-salvavidas-tipo-II.jpg";
import chalecoTipoIII from "../images/chaleco-salvavidas-tipo-III.jpg";
import chalecoTipoIV from "../images/chaleco-salvavidas.png";

const useStyles = makeStyles({
  cardListContainer: {
    maxWidth: "80vw",
    margin: "0 auto",
    padding: "1rem 0"
  },
  cardListTitle: {
    // margin: "1rem 0",
    color: "#0052CC",
    textAlign: "center",
    fontSize: "2rem",
    margin: "0",
    padding: "1rem"
  }
});

const chalecos = [
  {
    key: 1,
    title: "Tipo I",
    text:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Unde fugit dicta quis soluta quod quam labore temporibus blanditiis, ea numquam.",
    img: chalecoTipoI,
    alt: "chaleco tipo I"
  },
  {
    key: 2,
    title: "Tipo II",
    text:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Unde fugit dicta quis soluta quod quam labore temporibus blanditiis, ea numquam.",
    img: chalecoTipoII,
    alt: "chaleco tipo II"
  },
  {
    key: 3,
    title: "Tipo III",
    text:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Unde fugit dicta quis soluta quod quam labore temporibus blanditiis, ea numquam.",
    img: chalecoTipoIII,
    alt: "chaleco tipo III"
  },
  {
    key: 4,
    title: "Tipo IV",
    text:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Unde fugit dicta quis soluta quod quam labore temporibus blanditiis, ea numquam.",
    img: chalecoTipoIV,
    alt: "chaleco tipo IV"
  }
];

export default function CardList(props) {
  const classes = useStyles();

  return (
    <div style={{ backgroundColor: "#F4F5F7  " }}>
      <div className={classes.cardListContainer}>
        <h2 className={classes.cardListTitle}>Productos</h2>
        {chalecos.map(props => (
          <Card id={`tipo${props.key}`} info={props} />
        ))}
      </div>
    </div>
  );
}
