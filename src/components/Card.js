import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { CurrencyUsd } from "mdi-material-ui";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
  card: {},
  cardContainer: {
    height: "40vh",
    display: "flex",
    justifyContent: "space-around",
    backgroundColor: "white",
    borderRadius: "10px",
    margin: "1rem 0 0 0 ",
    "&:hover": {
      boxShadow: "0 2px 5px #7A869A"
    }
    // marginTop: "1rem"
    // padding: "5rem"
  },
  cardImgI: {
    maxWidth: "200px",
    maxHeight: "200px",
    display: "block",
    margin: "auto 0"
  },
  cardImgII: {
    minWidth: "30%",
    minHeight: "100%",
    display: "none"
  },
  cardInfo: {
    display: "flex",
    alignItems: "center",
    minHeight: "80%",
    minWidth: "30%",
    maxWidth: "60%",
    // border: "solid 1px black",
    fontFamily: "Roboto, sans serif"
  },
  cardInfoContainer: {
    maxHeight: "80%"
  },
  cardTitle: {
    color: "#FF8B00",
    margin: "0 0 1rem 0",
    fontSize: "30px"
  },
  cardButton: {
    color: "#00875A ",
    border: "solid 2px #00875A;",
    padding: "2px 5px",
    margin: "1.5rem 0"
  }
  // carLink: {
  //   margin: "1.5rem 0"
  // }
  // cardLink: {
  //   textDecoration: "none",
  //   color: "#008DA6",
  //   display: "flex",
  //   alignItems: "center;"
  // }
}));

// document.getElementById("myDIV").style.display = "none";

// export default function Card(props) {
//   const classes = useStyles();

//   function parImpar(styles, props) {
//     if (props % 2 == 0) {
//       styles.display = "block";
//     } else {
//       styles.display = "block";
//     }
//     return;
//   }

// const parImpar = (styles, props) => {
//   if (props % 2 === 0) {
//     return styles = {
//       display: "block"
//     };
//   } else {
//     return styles = {
//       display: "block"
//     };
// document.getElementsByClassName(`${useStyles.cardImgII}`).display =
//   "block";
//   }
// };

export default function Card(props) {
  const classes = useStyles();
  return (
    <div className={classes.card}>
      <div className={classes.cardContainer}>
        <img
          className={classes.cardImgI}
          src={props.info.img}
          alt={props.altName}
          // style={parImpar(classes.cardImgI, props.info.key)}
        />
        <div className={classes.cardInfo}>
          <div className={classes.cardInfoContainer}>
            <h2 className={classes.cardTitle}>{props.info.title}</h2>
            <p style={{ fontSize: "large" }}>{props.info.text}</p>
            {/* <Link className={classes.cardLinkTipo}>Ver Chaleco {props.info.title}</Link> */}
            <Link to={`/precios#${props.id}`} className={classes.cardLink}>
              <Button variant="outlined" className={classes.cardButton}>
                <CurrencyUsd />
                Ver Precio
              </Button>
            </Link>
            <div>
              <hr style={{ margin: "0" }} />
              <p style={{ fontSize: "15px", fontWeight: "500" }}>
                ¿Lo prefieres
                <strong style={{ color: "#008DA6" }}> personalizado</strong>?
                Llamanos
              </p>
            </div>
          </div>
        </div>
        {/* <img
        className={classes.cardImgII}
        src={props.info.img}
        alt={props.altName}
        // style={parImpar(classes.cardImgII, props.info.key)}
      /> */}
      </div>
    </div>
  );
}
