import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import sea from "../images/mar.jpg";

// const useStyles = makeStyles(theme => ({
//   headerContainer: {
//     sea: {
//       backgroundImage: `url(${sea})`,
//       backgroundSize: "cover",
//       height: "100vh"
//     }
//   }
// }));

const useStyles = makeStyles(theme => ({
  headerContainer: {
    height: "70vh",
    backgroundImage: `url(${sea})`,
    backgroundSize: "cover",
    backgroundAttachment: "fixed",
    color: "white",
    fontFamily: "Roboto, sansa-serif;",
    fontSize: "1.5rem",
    fontWeight: "500",
    textAlign: "center",
    padding: "5rem",
    lineHeight: "3rem"
  }
}));

export default function Header() {
  const classes = useStyles();
  return (
    <div className={classes.headerContainer}>
      <h1 style={{ margin: "2rem 0" }}>¿Por que nosotros?</h1>
      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam
      praesentium fugit asperiores placeat labore distinctio quibusdam modi
      quidem atque! Ad sed distinctio fuga corrupti dicta quasi aliquam,
      nesciunt rerum architecto deserunt blanditiis ab iusto, mollitia
      voluptates corporis temporibus reiciendis, ipsam expedita veniam nostrum
      ex exercitationem molestiae porro. Minus porro impedit rerum, laudantium
      ipsam rem fuga repellat culpa, enim cum et!
    </div>
  );
}
