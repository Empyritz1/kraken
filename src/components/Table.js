import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(theme => ({
  root: {
    width: "90%",
    margin: "1rem auto",
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  },
  tableTitle: {
    textAlign: "center"
  }
}));

export default function Tabla(props) {
  const classes = useStyles();

  return (
    <div>
      <h2 className={classes.tableTitle}>{props.title}</h2>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              {props.headCells.map(cell => (
                <TableCell align="center">{cell}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {props.data.map(row => (
              <TableRow>
                <TableCell component="th" scope="row" align="center">
                  {row[0]}
                </TableCell>
                <TableCell align="center">{row[1]}</TableCell>
                <TableCell align="center">{row[2]}</TableCell>
                <TableCell align="center">{row[3]}</TableCell>
                <TableCell align="center">{row[4]}</TableCell>
                {/* {row.map(data => (
                <TableCell>{data}</TableCell>
              ))} */}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}

// import React from "react";

// function createTable(table) {
//   table.map((value, index) => {
//     if (index === [0]) {
//       return (
//         <thead>
//           <tr>
//             {value.map(cell => {
//               return <th>{cell}</th>;
//             })}
//           </tr>
//         </thead>
//       );
//     } else {
//       return (
//         <tbody>
//           <tr>
//             {value.map(cell => {
//               return <td>{cell}</td>;
//             })}
//           </tr>
//         </tbody>
//       );
//     }
//   });
// }
// const tipoI = [
//   {
//     th: ["Tallas", "Kg", "Un color", "Dos Colores", "Camuflado militar"]
//   },
//   {
//     td: ["Baby - Bebe", "15", "$173.00", "$189.00", "184.00"]
//   },
//   {
//     td: ["Kid - Niño", "30", "$173.00", "$189.00", "184.00"]
//   },
//   {
//     td: ["XS - ECH", "45", "$189.00", "$206.00", "$200.00"]
//   },
//   {
//     td: ["S - CH", "60", "$200.00", "$216.00", "$211"]
//   }
// ];

// export default function TablaPrecios(props) {
//   return (
//     <div>
//       <table>{createTable(tipoI)}</table>
//       {/* <table>
//         {tipoI.map((value, index, table)=>({
//           if(index === [0]) {
//             <tr>
//               {value.map(value => (
//                 <td>{value}</td>
//               ))}
//             </tr>
//           }
//         }))}
//       </table> */}
//     </div>
//   );
// }

// //   if(table === [0]){(
// //     <tr>
// //       {table.map(row => (
// //         row.map(cell => (
// //           <th>{cell}</th>
// //         ))
// //       ))}
// //     </tr>
// //   )} else {

// //   }
// // )}
