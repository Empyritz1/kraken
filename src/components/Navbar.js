import React from "react";

import { Link } from "react-router-dom";

import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import kraken from "../images/logo_ck.png";

const useStyles = makeStyles(theme => ({
  button: {
    color: "white",
    fontFamily: "Roboto, sans serif;",
    margin: "0"
  },
  input: {
    display: "none"
  },
  header: {
    width: "100%",
    height: "auto",
    background: "#172B4D",
    color: "white",
    display: "flex",
    justifyContent: "space-around",
    fontSize: "bold",
    alignItems: "center"
  },
  logo: {
    display: "flex"
  },
  logoTitle: {
    margin: "auto .5rem",
    color: "white"
  },
  logoImg: {
    margin: "10px",
    width: "40px",
    height: "50px"
  }
}));

export default function(props) {
  const classes = useStyles();
  return (
    <Box boxShadow={10}>
      <div className={classes.header}>
        <Link to="/" style={{ textDecoration: "none" }}>
          <div className={classes.logo}>
            <img
              className={classes.logoImg}
              src={kraken}
              alt="Kraken"
              // width="70px"
              // height="60px"
            />
            <h2 className={classes.logoTitle}>{props.title}</h2>
          </div>
        </Link>
        <div>
          <ul className="links">
            {props.menu_items.map(menu_item => (
              <Link to={menu_item.path}>
                <Button className={classes.button}>
                  <li>{menu_item.name}</li>
                </Button>
              </Link>
            ))}
          </ul>
        </div>
      </div>
    </Box>
  );
}
