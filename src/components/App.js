import React, { Component } from "react";

import Layout from "./Layout";
import Home from "../pages/Home";
import Precios from "../pages/Precios";
import Nosotros from "../pages/Nosotros";
import Contacto from "../pages/Contacto";
import Manual from "../pages/Manual";

import { BrowserRouter, Switch, Route } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/precios" component={Precios} />
            <Route exact path="/nosotros" component={Nosotros} />
            <Route exact path="/contacto" component={Contacto} />
            <Route exact path="/manual" component={Manual} />
          </Switch>
        </Layout>
      </BrowserRouter>
    );
  }
}

export default App;
