import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles(theme => ({
  footerContainer: {
    // backgroundColor: "#548404",
    backgroundColor: "#00875A",
    // position: "absolute",
    // bottom: "0",
    width: "100%",
    color: "white",
    padding: "1rem 0"
  },
  icon: {
    color: "white",
    backgroundColor: "#333"
  },
  socialesContainer: {
    display: "flex",
    justifyContent: "center"
  },
  sociales: {
    display: "flex",
    listStyle: "none",
    textDecoration: "none",
    color: "white",
    margin: "0 10px 10px 0"
  },
  legal: {
    textAlign: "center"
  }
}));

export default function Footer(props) {
  const currentYear = new Date().getFullYear();
  const classes = useStyles();
  return (
    <div className={classes.footerContainer}>
      <h3 style={{ textAlign: "center", marginTop: "0" }}>Contactanos</h3>
      <div className={classes.socialesContainer} style={{ display: "flex" }}>
        {props.contact_items.map(item => (
          <a
            className={classes.sociales}
            href={item.link}
            target="_blank"
            rel="noopener noreferrer"
          >
            {/* <img className="img_link" src={item.img} alt="item" */}
            <Avatar className={classes.icon}>{item.img} </Avatar>
            {/* <p className={classes.p}>{item.info}</p> */}
          </a>
        ))}
      </div>
      <div className={classes.legal}>
        &copy; {currentYear} | Todos los derechos reservados
      </div>
    </div>
  );
}
