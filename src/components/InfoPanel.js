import React from "react";
import {
  makeStyles,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails
} from "@material-ui/core";
// import ExpansionPanel from '@material-ui/core/ExpansionPanel';
// import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
// import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

const useStyles = makeStyles({
  panelInfo: {
    display: "block"
  },
  infoPanelContainer: {
    margin: "0 1.2rem"
  }
});

export default function InfoPanel(props) {
  const classes = useStyles();
  return (
    <div className={classes.infoPanelContainer}>
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <h2>Cuidados</h2>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.panelInfo}>
          <h3>Antes de uso:</h3>
          -No altere un PFD para que se ajuste. Obtener una que se adapte. Un
          chaleco salvavidas aprobado no está alterado. <br />
          -Compruebe su PFD y asegúrese que no tenga rasgaduras, roturas y
          agujeros. Compruebe que las costuras, correas y componentes están en
          buena forma. Tire de las correas para asegurarse de que son seguras.{" "}
          <br />
          -Compruebe que no hay anegamiento (agua en su interior), olor a moho o
          encogimiento. Estos son indicios de pérdida de flotabilidad. <br />
          <h3>Durante el uso:</h3>
          -No use un PFD como un colchón, cojín de rodillas o parachoques del
          barco (Se perdera flotabilidad). -No coloque objetos pesados en los
          bolsillos. -Tenga cuidado de no poner objetos en los bolsillos que
          puedan perforar. <br />
          -Enjuague con agua dulce después de su uso, especialmente después de
          estar en agua salada. -Deje secar antes de guardarla de preferencia a
          la sombra. <br />
          <h3>Despues de su uso:</h3>
          -Enjuague con agua dulce después de su uso, especialmente después de
          estar en agua salada. <br />
          -No deje un PFD tumbado al sol durante largos períodos, deje secar
          antes de guardarla de preferencia a la sombra. <br />
          -Almacenar en un lugar fresco y seco, oscuro, donde hay una buena
          ventilación. <br />
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <h2>Guía de uso</h2>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails className={classes.panelInfo}>
          <h3>Tamaño</h3>
          Para los adultos, el tamaño de su pecho, su peso y altura,
          determinarán qué tamaño es el adecuado. Un PFD debe ser cómodo y
          encajar como un guante, sin embargo, permitirá moverse libremente.
          Para obtener la mejor sensación y ajuste, use ropa debajo. Cada PFD
          tendrá un diseño y modo de colocación diferente para adaptarse a los
          contornos del cuerpo. la colocación de cada modelo tiene más que ver
          con la comodidad que la seguridad. Las correas de un PFD se pueden
          ajustar para personalizar su ajuste.
          <h3>Montaje:</h3>
          -Afloje todas las correas, poner el PFD como cualquier chaleco. <br />
          -Comience en la cintura y apretar todas las correas. Si tiene correas
          para los hombros, apretarlos al final. Se debe sentir bien ajustado,
          pero no incómodo. <br />
          -A continuación, haga que alguien tire de la parte de arriba sobre los
          hombros del PFD. Si se mueve hasta más allá de su nariz o la cabeza,
          apretar más las correas. Si aun así se mueve hacia arriba, el PFD es
          demasiado grande. <br />
          -Ver sus movimientos para asegurarse de que es cómodo y no le roce,
          mientras que realice sus actividades. Hacer esto fuera del agua,
          simular que está realizando la actividad paro lo que necesita el
          chaleco. Esto estimulará lo que se siente al estar sentado en canoa o
          kayak, nadando, de pie, acostado, etc. <br />
          -Si es posible, pruebe su PFD en una piscina o en aguas poco profundas
          para ver cómo funciona. No deben moverse hacia arriba o deslizarse
          sobre la barbilla mientras flota. <br />
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <h2>Preguntas Frecuentes FAQs</h2>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.panelInfo}>
          P: ¿Cuánto tiempo dura un chaleco salvavidas? <br />
          R: No hay límite de tiempo estándar, aunque el cuidado adecuado hará
          que dure más tiempo. Los materiales saturados de agua, materiales
          desgastados, olor a moho o contracción de flotabilidad indican la
          necesidad de reemplazo. <br /> <br />
          P: ¿Una persona que sabe nadar necesita un PFD o sólo los que no saben
          nadar? <br />
          R: Todas las personas cuentan con una flotabilidad natural
          proporcionada por el mismo cuerpo, sin embargo, todos lo necesitan,
          por seguridad. <br />
          Extra: La mayoría de los adultos necesitan solamente usar el TIPO III
          para actividades acuáticas en zonas en las que el rescate sea rápido.
          Proporcionan flotabilidad extra para mantenerse a flote, ayudan
          siempre, por lo que incluso los nadadores deben usar un PFD que les
          ayude a salir rápidamente del agua y ponerse a salvo, si quedara
          inconsciente, por algún motivo, el chaleco salvavidas auxiliaría en su
          rescate. <br /> <br />
          P: ¿Con qué frecuencia se debe probar si funciona bien un PFD? <br />
          R: Por lo menos una vez al año (si su uso es constante). Si esta
          anegado, descolorido o con fugas, un PFD debe desecharse adecuadamente
          y destruirse para que otra persona no utilice un modelo viejo e
          inservible. <br /> <br />
          P: ¿Un PFD me protege de la hipotermia? <br />
          R: La hipotermia no puede evitarse simplemente con el uso de un PFD.
          Un traje especial de inmersión debe ser usado bajo un PFD en aguas
          frías, pero la hipotermia es siempre un riesgo si se encuentra en agua
          helada o expuesto a un clima frío.
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
}
