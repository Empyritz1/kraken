import React from "react";
import {
  Table,
  TableHead,
  TableBody,
  TableFooter,
  TableRow,
  TableCell
} from "@material-ui/core";

export default function Pricing() {
  return (
    <div>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody />
        <TableFooter />
      </Table>
    </div>
  );
}
