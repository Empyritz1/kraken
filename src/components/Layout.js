import React from "react";
import Footer from "./Footer";
import Navbar from "./Navbar";

import { Whatsapp, Facebook, Email, MapMarker } from "mdi-material-ui";

export default function Layout(props) {
  var menu = [
    {
      name: "Nosotros",
      path: "/nosotros"
    },
    {
      name: "Precios",
      path: "/precios"
    },
    {
      name: "Manual",
      path: "/manual"
    },
    {
      name: "Contactanos",
      path: "/contacto"
    }
  ];
  var contact_items = [
    {
      link:
        "https://www.facebook.com/chalecossalvavidaskraken/?__tn__=%2Cd%2CP-R&eid=ARBp9A9JYlQiiETUTFKDAh4KQt8Qtha1ZyeTbrPLADeIFUms1oazsKEuElKERm-E_SYV0gPz5gytYhwH",
      img: <Facebook />,
      info: "Chalekos Kraken"
    },
    {
      link:
        "https://api.whatsapp.com/send?phone=+525512580055&text=Hola, que tal.",
      img: <Whatsapp />,
      info: "+55 12580055"
    },
    {
      link: "mailto:chalecoskraken@gmail.com",
      img: <Email />,
      info: "chalecoskraken@hotmail.com"
    },
    {
      link: "https://goo.gl/maps/TWzZQS4imHzZ2P4h6",
      img: <MapMarker />,
      info: "Juan Colorado #120"
    }
  ];

  return (
    <>
      <Navbar title="Chalecos Kraken" menu_items={menu} />
      {props.children}
      <Footer contact_items={contact_items} />
    </>
  );
}
