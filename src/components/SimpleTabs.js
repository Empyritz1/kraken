import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
// import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      <Box>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    height: "90vh"
  },
  tabBarContainer: {},
  tabBar: {
    backgroundColor: "#FF8F73",
    color: "white"
  },
  tabBarItem: {
    maxWidth: "60%",
    margin: "0 auto",
    display: "flex",
    justifyContent: "space-evenly"
  },
  tabInfo: {
    maxWidth: "60%",
    maxHeight: "80%",
    margin: "auto auto",
    fontFamily: "Roboto, sans serif",
    fontSize: "1.3rem",
    lineHeight: "3rem"
  },
  titleContainer: {
    display: "flex",
    // justifyContent: "space-evenly",
    alignItems: "center",
    margin: "4rem 0 3rem 0"
  },
  title: {
    margin: "0 1rem"
  }
}));

export default function SimpleTabs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  return (
    <div className={classes.root}>
      <Tabs
        value={value}
        onChange={handleChange}
        aria-label="simple tabs example"
        className={classes.tabBar}
      >
        {props.info.map((value, index, object) => (
          <Tab
            label={value.label}
            {...a11yProps`(${index})`}
            className={classes.tabBarItem}
          />
        ))}
      </Tabs>
      {props.info.map((info, index) => (
        <TabPanel value={value} index={index} className={classes.tabInfo}>
          <div className={classes.titleContainer}>
            {info.icon}
            <h2 className={classes.title}>{info.title}</h2>
          </div>
          <p>{info.text}</p>
        </TabPanel>
      ))}
    </div>
  );
  // return (
  //   <div className={classes.root}>
  //     <AppBar position="static">
  //       <Tabs
  //         value={value}
  //         onChange={handleChange}
  //         aria-label="simple tabs example"
  //       >
  //         <Tab label="Item One" {...a11yProps(0)} />
  //         <Tab label="Item Two" {...a11yProps(1)} />
  //         <Tab label="Item Three" {...a11yProps(2)} />
  //       </Tabs>
  //     </AppBar>
  //     <TabPanel value={value} index={0}>
  //       Item One
  //     </TabPanel>
  //     <TabPanel value={value} index={1}>
  //       Item Two
  //     </TabPanel>
  //     <TabPanel value={value} index={2}>
  //       Item Three
  //     </TabPanel>
  //   </div>
  // );
}

// import React from "react";
// import { makeStyles } from "@material-ui/core/styles";
// import Grid from "@material-ui/core/Grid";
// import Button from "@material-ui/core/Button";

// function handleClick() {
//   const element = document.getElementById("ocultable");
//   element.style.display = "block";
// }

// let useStyles = makeStyles({
//   ocultable: {
//     display: "none"
//   }
// });

// export default function TextHidden() {
//   const classes = useStyles();
//   return (
//     <Grid container>
//       <Grid item>
//         <Button onClick={handleClick()} />
//       </Grid>
//       <Grid item className={classes.ocultable} id="ocultable">
//         <p>
//           Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque minus
//           laudantium, ratione inventore itaque libero ut numquam et dolor
//           similique?
//         </p>
//       </Grid>
//     </Grid>
//   );
// }

// {/* <button onclick={mostrarOcultar("ocultable")} type="button">
//   Mostrar/Ocultar
// </button>
// <div id="ocultable" className={classes.ocultable}>
//   Soy un texto ocultable.
// </div> */}

/* 
// function mostrarOcultar(id) {
//   var elemento = document.getElementById(id);
//   if (!elemento) {
//     return true;
//   }
//   if (elemento.style.display == "none") {
//     elemento.style.display = "block";
//   } else {
//     elemento.style.display = "none";
//   }
//   return true;
// } */
