import React from "react";
import ContactCard from "./ContactCard";
import { makeStyles } from "@material-ui/core";
import rio from "../images/rio.jpg";
import { Clock } from "mdi-material-ui";

const useStyles = makeStyles({
  contactBody: {
    height: "100vh",
    backgroundImage: `url(${rio})`,
    backgroundSize: "cover",
    backgroundAttachment: "fixed",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  contactBodyContainer: {
    width: "80%",
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center"
  },
  contactInfo: {
    color: "white",
    fontFamily: "Roboto, sans serif",
    fontSize: "1.1rem",
    color: "white",
    textAlign: "center"
  },
  contactInfoClock: {
    display: "flex",
    justifyContent: "space-evenly",
    alignItems: "center"
  }
});

export default function ContactBody() {
  const classes = useStyles();
  return (
    <div className={classes.contactBody}>
      <div className={classes.contactBodyContainer}>
        <div className={classes.contactInfo}>
          <h3 className={classes.contactInfoClock}>
            <Clock />
            Horarios de atención
          </h3>
          <p>Lunes - Viernes: 11:00 am - 10:00 pm</p>
          <p>Sabado: 12:00 am - 8:00 pm</p>
        </div>
        <ContactCard />
      </div>
    </div>
  );
}
