import React from "react";
import { FormControl, InputLabel, Input, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { Send } from "mdi-material-ui";

const useStyles = makeStyles({
  contactCard: {
    width: "35vw",
    height: "70vh",
    backgroundColor: "#fff",
    borderRadius: "10px",
    display: "flex",
    justifyContent: "center"
  },
  contactCardContainer: {
    height: "100%",
    width: "80%"
  },
  contactCardTitle: {
    height: "16%",
    width: "100%",
    backgroundColor: "#5243AA",
    color: "white",
    borderRadius: "5px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontFamily: "Roboto, sans serif",
    fontSize: "1.4rem",
    fontWeight: "bold",
    marginTop: "-3rem",
    boxShadow: "0 3px 5px 0 #8777D9"
  },
  contactCardName: {
    display: "flex",
    justifyContent: "space-between",
    paddingTop: "2rem",
    marginBottom: "1rem"
  },
  contactCardNameItem: {
    width: "45%"
  },
  contactCardEmail: {
    width: "100%",
    paddingTop: "2rem",
    marginBottom: "1rem"
  },
  contactCardText: {
    width: "100%"
  },
  contactCardSubmit: {
    margin: "2rem auto"
  }
});

export default function ContactCard() {
  const classes = useStyles();
  return (
    <div className={classes.contactCard}>
      <div className={classes.contactCardContainer}>
        <div className={classes.contactCardTitle}>Contactanos</div>
        {/* <form action="pagina a la que se va a mandar" method="get"> */}
        <div className={classes.contactCardName}>
          <FormControl className={classes.contactCardNameItem}>
            <InputLabel htmlFor="first-name">Nombre</InputLabel>
            <Input id="first-name" />
          </FormControl>
          <FormControl className={classes.contactCardNameItem}>
            <InputLabel htmlFor="last-name">Apellido</InputLabel>
            <Input id="last-name" />
          </FormControl>
        </div>
        <div>
          <FormControl className={classes.contactCardEmail}>
            <InputLabel type="email" htmlFor="email">
              Email
            </InputLabel>
            <Input id="email" />
          </FormControl>
        </div>
        <div>
          <FormControl className={classes.contactCardText}>
            <TextField
              id="standard-multiline-flexible"
              label="Text"
              multiline
              rows="4"
              // value={values.multiline}
              // onChange={handleChange("multiline")}
              className={classes.textField}
              margin="normal"
            />
            {/* <InputLabel htmlFor="text">Text</InputLabel>
            <Input rows="10" id="my-input" aria-describedby="my-helper-text" /> */}
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className={classes.contactCardSubmit}
            >
              Enviar
              <Send style={{ marginLeft: "10px" }} />
            </Button>
          </FormControl>
        </div>
      </div>
    </div>
  );
}
